package net.snuckdev.archaicreparar;

import net.milkbowl.vault.economy.Economy;
import net.snuckdev.archaicreparar.commands.SetBlacksmithCommand;
import net.snuckdev.archaicreparar.listeners.BlacksmithListener;
import net.snuckdev.archaicreparar.runnable.BlacksmithRunnable;
import net.snuckdev.archaicreparar.utils.CommentYamlConfiguration;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.io.File;


public class Main extends CommentYamlConfiguration.JavaPlugin {

    public static Main plugin;
    public static Economy econ = null;

    @Override
    public void onEnable() {
        plugin = this;
        if(setupEconomy()) {
            Bukkit.getConsoleSender().sendMessage("§e[SnuckReparar] §aVault encontrado.");
        } else {
            Bukkit.getConsoleSender().sendMessage("§e[SnuckReparar] §cVault não encontrado.");
        }
        saveDefaultConfig();
        CommentYamlConfiguration.loadConfiguration(new File(getDataFolder(), "config.yml"));
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new BlacksmithListener(), this);
        getCommand("setferreiro").setExecutor(new SetBlacksmithCommand());
        BlacksmithRunnable.sendBlacksmithAnnounces(getConfig().getString("location.world"));
        Bukkit.getConsoleSender().sendMessage("§e[SnuckReparar] §aHabilitado com sucesso.");
        super.onEnable();
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    @Override
    public void onDisable() {
        Bukkit.getConsoleSender().sendMessage("§c[ArchaicReparar] Desabilitado com sucesso.");
        super.onDisable();
    }
}
