package net.snuckdev.archaicreparar.runnable;

import net.snuckdev.archaicreparar.Main;
import net.snuckdev.archaicreparar.utils.ConfigUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class BlacksmithRunnable {

    public static void sendBlacksmithAnnounces(String world) {
        Random messages = new Random();
        new BukkitRunnable() {
            @Override
            public void run() {
                int message = messages.nextInt(Main.plugin.getConfig().getStringList("anuncios.anuncios").size());
                int range = Main.plugin.getConfig().getInt("anuncios.raio");
                World w = Bukkit.getWorld(world);
                if(w == null) {
                    Bukkit.getConsoleSender().sendMessage("§e[SnuckReparar] §cO mundo que está na config.yml não existe.");
                    Bukkit.getConsoleSender().sendMessage("§e[SnuckReparar] §cTente setar o NPC na localização da sua escolha" +
                            "e recarregue o plugin.");
                    return;
                }
                w.getEntities().forEach(en -> {
                    if (en instanceof Villager) {
                        if (en.getCustomName().equals(ConfigUtils.get("messages.nome-ferreiro"))) {
                            en.getNearbyEntities(range, range, range).forEach(p -> {
                                if (p instanceof Player) {
                                    p.sendMessage(Main.plugin.getConfig().getStringList("anuncios.anuncios").get(message)
                                            .replace("&", "§"));
                                }

                            });
                        }
                    }
                });
            }
        }.runTaskTimer(Main.plugin, 20L, 1200L * Main.plugin.getConfig().getInt("anuncios.tempo"));
    }

}
