package net.snuckdev.archaicreparar.utils;

import net.snuckdev.archaicreparar.Main;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigUtils {

    public static String get(String path) {
        FileConfiguration cf = CommentYamlConfiguration.loadConfiguration(new File(Main.plugin.getDataFolder(), "config.yml"));
        return cf.getString(path).replace("&", "§");
    }

    public static void saveLocation(Location loc) {
        File f = new File(Main.plugin.getDataFolder(), "config.yml");
        FileConfiguration cf = CommentYamlConfiguration.loadConfiguration(new File(Main.plugin.getDataFolder(), "config.yml"));
        double x = loc.getX();
        double y = loc.getY();
        double z = loc.getZ();
        float pitch = loc.getPitch();
        float yaw = loc.getYaw();
        String w = loc.getWorld().getName();
        cf.set("location.x", x);
        cf.set("location.y", y);
        cf.set("location.z", z);
        cf.set("location.pitch", pitch);
        cf.set("location.yaw", yaw);
        cf.set("location.world", w);
        try {
            cf.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
