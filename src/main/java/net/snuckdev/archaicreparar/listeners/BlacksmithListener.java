package net.snuckdev.archaicreparar.listeners;

import net.milkbowl.vault.economy.EconomyResponse;
import net.snuckdev.archaicreparar.Main;
import net.snuckdev.archaicreparar.items.ItemBuilder;
import net.snuckdev.archaicreparar.utils.ConfigUtils;
import net.snuckdev.archaicreparar.utils.Percentage;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BlacksmithListener implements Listener {

    boolean econ = Main.plugin.getConfig().getBoolean("economia.usar");
    int cost = Main.plugin.getConfig().getInt("economia.valor");

    @EventHandler
    public void on(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();
        Entity en = e.getRightClicked();
        String name = en.getCustomName();
        ItemStack hand = p.getItemInHand();
        Material handType = hand.getType();

        if (name.equals(ConfigUtils.get("messages.nome-ferreiro")) && en instanceof Villager) {
            if (hand == null || handType == Material.AIR) {
                p.sendMessage(ConfigUtils.get("messages.precisa-de-item"));
                e.setCancelled(true);
                return;
            }
            e.setCancelled(true);
            Inventory inv = Bukkit.createInventory(null, 9 * 3, ConfigUtils.get("inventario.title"));

            ItemStack confirm = new ItemBuilder(Material.WOOL, 1, 13)
                    .setDisplayName(ConfigUtils.get("inventario.confirmar.name"))
                    .setLore(Arrays.asList("§7Há chances do seu item não",
                            "§7ser encantado com sucesso, tome cuidado!"))
                    .build();

            if (econ) {
                ItemMeta confirmMeta = confirm.getItemMeta();
                List<String> lore = new ArrayList<>();
                lore.add("");
                lore.add("§7Valor: §f" + cost);
                confirmMeta.setLore(lore);
                confirm.setItemMeta(confirmMeta);
            }

            ItemStack cancel = new ItemBuilder(Material.WOOL, 1, 14)
                    .setDisplayName(ConfigUtils.get("inventario.cancelar.name"))
                    .setLore(Arrays.asList("§7Fazendo isso você irá",
                            "§7cancelar o processo de reparação."))
                    .build();
            inv.setItem(10, confirm);
            inv.setItem(13, p.getItemInHand());
            inv.setItem(16, cancel);
            p.sendMessage(ConfigUtils.get("messages.ferreiro-mensagem"));
            p.openInventory(inv);
        }
    }

    @EventHandler
    public void on(EntityDamageByEntityEvent e) {
        Entity damaged = e.getEntity();
        Entity damager = e.getDamager();
        Player p = (Player) damager;
        ItemStack hand = p.getItemInHand();
        Material type = hand.getType();
        String damagedName = damaged.getCustomName();

        if (damager instanceof Player) {
            if (type == Material.BLAZE_ROD && damaged instanceof Villager
                    && damagedName.equals(ConfigUtils.get("messages.nome-ferreiro"))) {
                ((Villager) damaged).setHealth(0);
            }
        }
    }

    @EventHandler
    public void on(InventoryClickEvent e) {
        String title = e.getInventory().getTitle();
        ItemStack item = e.getCurrentItem();
        String name = item.getItemMeta().getDisplayName();
        Player p = (Player) e.getWhoClicked();
        Material itemType = item.getType();

        if (title.equals(ConfigUtils.get("inventario.title"))) {
            e.setCancelled(true);
            if (null == item) return;
            if (itemType == Material.AIR) return;
            if (name.equals(ConfigUtils.get("inventario.confirmar.name"))) {
                e.setCancelled(true);

                if (!(Percentage.percentChance(50))) {
                    if (econ) {
                        if (Main.econ.getBalance(p) >= cost) {
                            Main.econ.withdrawPlayer(p, cost);
                            p.sendMessage(ConfigUtils.get("messages.ferreiro-errado-mensagem"));
                            ItemStack itemP = p.getItemInHand();
                            ItemMeta itemPM = itemP.getItemMeta();
                            itemP.setItemMeta(itemPM);
                            p.setItemInHand(itemP);
                            p.closeInventory();
                        } else {
                            p.sendMessage(ConfigUtils.get("economia.nao-tem-money"));
                        }
                        p.closeInventory();
                    } else {
                        p.sendMessage(ConfigUtils.get("messages.ferreiro-errado-mensagem"));
                        ItemStack itemP = p.getItemInHand();
                        ItemMeta itemPM = itemP.getItemMeta();
                        itemP.setItemMeta(itemPM);
                        p.setItemInHand(itemP);
                        p.closeInventory();
                    }
                    return;
                }

                if (econ) {
                    if (Main.econ.getBalance(p) >= cost) {
                        Main.econ.withdrawPlayer(p, cost);
                        p.sendMessage(ConfigUtils.get("messages.ferreiro-certo-mensagem"));
                        ItemStack itemP = p.getItemInHand();
                        ItemMeta itemPM = itemP.getItemMeta();
                        itemP.setItemMeta(itemPM);
                        itemP.setDurability((short) 0);
                        p.setItemInHand(itemP);
                    } else {
                        p.sendMessage(ConfigUtils.get("economia.nao-tem-money"));
                    }
                    p.closeInventory();
                } else {
                    p.sendMessage(ConfigUtils.get("messages.ferreiro-certo-mensagem"));
                    ItemStack itemP = p.getItemInHand();
                    ItemMeta itemPM = itemP.getItemMeta();
                    itemP.setItemMeta(itemPM);
                    itemP.setDurability((short) 0);
                    p.setItemInHand(itemP);
                    p.closeInventory();
                }
            } else if (name.equals(ConfigUtils.get("inventario.cancelar.name"))) {
                p.closeInventory();
            }

        }
    }
}
