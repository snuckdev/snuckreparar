package net.snuckdev.archaicreparar.commands;

import net.snuckdev.archaicreparar.runnable.BlacksmithRunnable;
import net.snuckdev.archaicreparar.utils.ConfigUtils;
import net.snuckdev.archaicreparar.utils.NoAI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

public class SetBlacksmithCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {

        if(!(sender instanceof Player)) {
            return false;
        }
        Player p = (Player) sender;
        if(!(p.hasPermission("archaicreparar.admin"))) {
            p.sendMessage(ConfigUtils.get("messages.sem-perm"));
            return false;
        }
        Villager v = (Villager) p.getWorld().spawnEntity(p.getLocation(), EntityType.VILLAGER);
        v.setProfession(Villager.Profession.BLACKSMITH);
        v.setCustomName(ConfigUtils.get("messages.nome-ferreiro"));
        v.setAge(100);
        v.setAgeLock(true);
        v.setCustomNameVisible(true);
        ConfigUtils.saveLocation(p.getLocation());
        NoAI.setAiEnabled(v, false);
        p.sendMessage(ConfigUtils.get("messages.ferreiro-setado"));
        return false;
    }
}
